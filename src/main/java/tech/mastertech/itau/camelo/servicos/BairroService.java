package tech.mastertech.itau.camelo.servicos;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.transaction.Transactional;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tech.mastertech.itau.camelo.entidades.Bairro;
import tech.mastertech.itau.camelo.modelos.BairroImportacao;
import tech.mastertech.itau.camelo.repositorios.BairroRepository;

@Service
public class BairroService {

	@Autowired
	private BairroRepository bairroRespository;

	@Transactional
	public void salvarBairro(BairroImportacao novoBairro) {
		Bairro bairro = novoBairro.criarBairro();
		bairroRespository.save(bairro);
	}

	@Transactional
	public void importarBairro(File arquivo) throws IOException {
		CSVParser csv = CSVFormat.DEFAULT.parse(new FileReader(arquivo));

		for (CSVRecord linha : csv) {
			BairroImportacao novoBairro = this.criarBairro(linha);

			this.salvarBairro(novoBairro);
		}
	}

	private BairroImportacao criarBairro(CSVRecord linha) {
		String nome = linha.get(0);
		return new BairroImportacao(nome);
	}
}