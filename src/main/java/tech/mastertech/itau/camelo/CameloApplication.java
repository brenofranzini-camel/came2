package tech.mastertech.itau.camelo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CameloApplication {

	public static void main(String[] args) {
		SpringApplication.run(CameloApplication.class, args);
	}

}
