package tech.mastertech.itau.camelo.repositorios;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.camelo.entidades.Bairro;

public interface BairroRepository extends CrudRepository<Bairro, Integer> {

}
