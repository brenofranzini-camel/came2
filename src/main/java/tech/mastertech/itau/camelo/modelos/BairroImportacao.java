package tech.mastertech.itau.camelo.modelos;

import tech.mastertech.itau.camelo.entidades.Bairro;

public class BairroImportacao {

	private String nome;


	public BairroImportacao(String nome) {
		this.nome = nome;
	}

	public Bairro criarBairro() {
		Bairro bairro = new Bairro();
		bairro.setNome(this.nome);

		return bairro;
	}
}