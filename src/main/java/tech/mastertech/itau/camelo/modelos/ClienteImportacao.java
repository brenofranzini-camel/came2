package tech.mastertech.itau.camelo.modelos;

import java.math.BigDecimal;
import java.time.LocalDate;

import tech.mastertech.itau.camelo.entidades.Bairro;
import tech.mastertech.itau.camelo.entidades.Cliente;

public class ClienteImportacao {

	private String nome;

	private LocalDate nascimento;

	private BigDecimal saldoInicial;

	private int idBairro;

	public ClienteImportacao(String nome, LocalDate nascimento, BigDecimal saldoInicial, int idBairro) {
		this.nome = nome;
		this.nascimento = nascimento;
		this.saldoInicial = saldoInicial;
		this.idBairro = idBairro;
	}

	public Cliente criarCliente() {
		Cliente cliente = new Cliente();
		cliente.setNome(this.nome);
		cliente.setNascimento(this.nascimento);
		cliente.setSaldoInicial(this.saldoInicial);

		Bairro bairro = new Bairro();
		bairro.setId(this.idBairro);
		cliente.setBairro(bairro);

		return cliente;
	}
}