package tech.mastertech.itau.camelo.rotas;

import java.io.File;

import org.apache.camel.Exchange;
import org.apache.camel.Predicate;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.GenericFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import tech.mastertech.itau.camelo.servicos.ClienteService;

@Component
public class ImportarClienteRoute extends RouteBuilder {
	@Autowired
	private ClienteService service;

	@Override
	public void configure() throws Exception {
		
		Predicate csvValido = new Predicate() {
			@Override
			public boolean matches(Exchange exchange) {
				return ImportarClienteRoute.this.service.isCsv(
						(File)((GenericFile) exchange.getIn().getBody()).getFile());
			}
		};

		from("file://origem/?noop=true")
		.routeId("rota-importacao")
		.choice()
		.when(csvValido)
		.process(new Processor() {
			
			@Override
			public void process(Exchange exchange) throws Exception {
				String nomeArquivo = exchange.getIn().getHeader(Exchange.FILE_PATH, String.class);
				
//				((GenericFile) exchange.getIn().getBody()).getFile();
				
				System.out.printf("\nTentando importar o arquivo %s \n", nomeArquivo);
			}
		})
		.to("bean:clienteService?method=importarCliente(${body.getFile()})")
		.otherwise()
		.log("Arquivo ${body.getAbsoluteFilePath()} não é um CSV válido!")
		.end();
	}
}